class CreatePictures < ActiveRecord::Migration
  def change
    create_table :pictures do |t|
      t.attachment :image
      t.string :title
      t.string :description

      t.timestamps null: false
    end
  end
end

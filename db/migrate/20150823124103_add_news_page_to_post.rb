class AddNewsPageToPost < ActiveRecord::Migration
  def change
    add_column :posts, :news_page, :boolean
  end
end

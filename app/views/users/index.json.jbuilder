json.array!(@users) do |user|
  json.extract! user, :id, :nickname, :name, :last_name, :avatar
  json.url user_url(user, format: :json)
end

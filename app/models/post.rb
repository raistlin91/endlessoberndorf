class Post < ActiveRecord::Base
  belongs_to :user
  validates :title, :body, presence: true
  has_attached_file :picture, styles: { medium: "300x300>", thumb: "100x100>" }
  has_attached_file :clip,:styles => {
                             :medium => { :geometry => "640x480", :format => 'ogg'},
                             :poster => { geometry: "640x480", format: 'jpg', time: 10},
                             :thumb => { :geometry => "100x100#", :format => 'jpg', :time => 15 }
                         }, :processors =>  [:transcoder],
                    :use_timestamp => false
  validates_attachment_content_type :picture, content_type: /\Aimage\/.*\Z/
  validates_attachment_content_type :clip, content_type: /\Avideo\/.*\Z/

end
class User < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable,:rememberable#, :registerable,
         #:recoverable,
         # :trackable, :validatable
  validates :name, :last_name, presence: true
  has_attached_file :avatar, styles: { medium: "300x300>", thumb: "100x100>" }, default_url: "avatar.jpg"
  validates_attachment_content_type :avatar, content_type: /\Aimage\/.*\Z/
  after_destroy :ensure_an_admin_remains
  after_update :ensure_an_admin_remains
  has_many :posts


  private
  def ensure_an_admin_remains
    if User.where(admin: true).count.zero?
      raise "Can't delete last admin"
    end
  end
end
